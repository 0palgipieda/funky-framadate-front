import { Component, Input, OnInit } from '@angular/core';
import { PollService } from '../../../../../core/services/poll.service';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { environment } from '../../../../../../environments/environment';
import { Title } from '@angular/platform-browser';

@Component({
	selector: 'app-step-three',
	templateUrl: './step-three.component.html',
	styleUrls: ['./step-three.component.scss'],
})
export class StepThreeComponent implements OnInit {
	@Input()
	step_max: any;
	@Input()
	form: any;
	public environment = environment;
	minDate: any = new Date();
	hideBackButton: boolean = true;

	onMonthChangeCustom($event: any) {
		// disable nav if new month is in past
		console.log('$event', $event);
	}

	constructor(public pollService: PollService, private titleService: Title) {
		this.hideBackButton = this.areWeOnCurrentMonth();

		this.pollService.step_current = 3;
		this.step_max = this.pollService.step_max;
	}

	ngOnInit(): void {}

	drop(event: CdkDragDrop<string[]>) {
		// moveItemInArray(this.pollService.choices, event.previousIndex, event.currentIndex);
	}
	convertDateInputs() {
		this.pollService.mode_calendar
			? this.pollService.convertCalendarToText()
			: this.pollService.convertTextToCalendar();
	}

	changeDateInputMode() {
		this.convertDateInputs();
		this.pollService.mode_calendar = !this.pollService.mode_calendar;
	}

	private areWeOnCurrentMonth() {
		return true;
	}
}
