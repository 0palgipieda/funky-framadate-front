import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoiceButtonDinumComponent } from './choice-button-dinum.component';

describe('ChoiceButtonDinumComponent', () => {
	let component: ChoiceButtonDinumComponent;
	let fixture: ComponentFixture<ChoiceButtonDinumComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ChoiceButtonDinumComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChoiceButtonDinumComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
